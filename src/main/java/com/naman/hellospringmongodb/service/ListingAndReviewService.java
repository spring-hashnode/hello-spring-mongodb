package com.naman.hellospringmongodb.service;

import com.naman.hellospringmongodb.entity.AirBNBListingAndReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.naman.hellospringmongodb.repository.AirBNBListingAndReviewRepository;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Service
public class ListingAndReviewService {

    @Autowired
    private AirBNBListingAndReviewRepository listingAndReviewRepo;

    public void getData() {

    }

    public List<AirBNBListingAndReview> getOptimized(int fromPrice, int toPrice, int page, int size) {
        return listingAndReviewRepo.findByPriceBetweenQuery(fromPrice, toPrice, PageRequest.of(page, size));
    }

    public List<AirBNBListingAndReview> searchSummary(String query, int page, int size) {
        return listingAndReviewRepo.findBySummary(query, PageRequest.of(page, size));
    }
}
