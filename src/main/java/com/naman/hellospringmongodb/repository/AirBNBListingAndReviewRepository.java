package com.naman.hellospringmongodb.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.naman.hellospringmongodb.entity.AirBNBListingAndReview;

@Repository
public interface AirBNBListingAndReviewRepository extends MongoRepository<AirBNBListingAndReview, String> {

    // find by 1 field
    List<AirBNBListingAndReview> findByPropertyType(String propertyType, Pageable pageRequest);

    // find by 1 field - @Query
    @Query(value = "{ 'propertyType': ?0 }")
    List<AirBNBListingAndReview> findByPropertyTypeQuery(String propertyType, Pageable pageRequest);

    // find by 2 fields
    List<AirBNBListingAndReview> findByPropertyTypeAndRoomType(String propertyType, String roomType, Pageable pageRequest);

    // find by 2 fields - @Query
    @Query(value = "{ 'propertyType': ?0, 'roomType': ?1 }")
    List<AirBNBListingAndReview> findByPropertyTypeAndRoomTypeQuery(String propertyType, String roomType, Pageable pageRequest);

    // find by - not equal to 1 field
    List<AirBNBListingAndReview> findByPropertyTypeNot(String propertyType, Pageable pageRequest);

    // find by - not equal to 1 field - @Query
    @Query(value = "{ 'propertyType': { '$ne': ?0 } }")
    List<AirBNBListingAndReview> findByPropertyTypeNotQuery(String propertyType, Pageable pageRequest);

    // find by 1 field and sorted by last_scraped - @Query
    @Query(value = "{ 'propertyType': { '$ne': ?0 } }", sort = "{ last_scraped: 1 }")
    List<AirBNBListingAndReview> findByPropertyTypeOrderByLastScrapedQuery(String propertyType, Pageable pageRequest);

    // find by 1 field and sorted by last_scraped in descending order
    List<AirBNBListingAndReview> findByPropertyTypeOrderByLastScrapedDesc(String propertyType, Pageable pageRequest);

    // find by 1 field and sorted by first_review in ascending order & last_scraped in descending order - @Query
    @Query(value = "{ 'propertyType': { '$ne': ?0 } }", sort = "{ first_review, last_scraped: -1 }")
    List<AirBNBListingAndReview> findByPropertyTypeOrderByLastScrapedDescQuery(String propertyType, Pageable pageRequest);

    // find by value between 2 field
    List<AirBNBListingAndReview> findByPriceBetween(double priceFrom, double priceTo, Pageable pageRequest);

    // find by value between 2 field - @Query
    @Query(value = "{ 'price': { '$gt': ?0, '$lt': ?1 } }")
    List<AirBNBListingAndReview> findByPriceBetweenQuery(double priceFrom, double priceTo, Pageable pageRequest);

    // find by name regex
    List<AirBNBListingAndReview> findByNameRegex(String firstname, Pageable pageRequest);

    // find by name regex - @Query
    @Query(value = "{ 'name': { '$regex': ?0, '$options': 'i' } }")
    List<AirBNBListingAndReview> findByNameRegexQuery(String firstname, Pageable pageRequest);

    // find by name regex - @Query
    @Query(value = "{ 'summary': { '$regex': ?0, '$options': 'i' } }")
    List<AirBNBListingAndReview> findBySummaryRegex(String firstname, Pageable pageRequest);

    // find by name text Index - @Query
    @Query(value = "{ $text: { $search: ?0 } }")
    List<AirBNBListingAndReview> findBySummary(String summary, Pageable pageRequest);
}
