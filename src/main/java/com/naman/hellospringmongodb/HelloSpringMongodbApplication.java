package com.naman.hellospringmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloSpringMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloSpringMongodbApplication.class, args);
	}

}
