package com.naman.hellospringmongodb.api;

import com.naman.hellospringmongodb.entity.AirBNBListingAndReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.naman.hellospringmongodb.service.ListingAndReviewService;

import java.util.List;

@RestController
@RequestMapping("/review")
public class ListingAndReviewApi {

    @Autowired
    private ListingAndReviewService listingAndReviewService;

    @GetMapping("/getData")
    public ResponseEntity<Void> getByPropertyType() {
        listingAndReviewService.getData();

        return ResponseEntity.ok().build();
    }

    @GetMapping("/getOptimized")
    public ResponseEntity<List<AirBNBListingAndReview>> getOptimized(
            @RequestParam int fromPrice,
            @RequestParam int toPrice,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize
    ) {
        return ResponseEntity.ok(listingAndReviewService.getOptimized(fromPrice, toPrice, pageNo, pageSize));
    }

    @GetMapping("/summary")
    public ResponseEntity<List<AirBNBListingAndReview>> searchSummary(
            @RequestParam String query,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize
    ) {
        return ResponseEntity.ok(listingAndReviewService.searchSummary(query, pageNo, pageSize));
    }
}
