package com.naman.hellospringmongodb.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@CompoundIndex
@Document("listingsAndReviews")
public class AirBNBListingAndReview {

    @Id
    private String id;

    @Field("last_scraped")
    private Date lastScraped;

    @Field("listing_url")
    private String listingUrl;

    @Indexed
    private String name;

    @TextIndexed
    private String summary;

    private String space;

    @TextIndexed
    private String description;

    @Field("neighborhood_overview")
    private String neighborhoodOverview;

    private String notes;

    private String transit;

    private String access;

    private String interaction;

    @Field("house_rules")
    private String houseRules;

    @Field("property_type")
    private String propertyType;

    private Double price;

    @Field("room_type")
    private String roomType;
}
